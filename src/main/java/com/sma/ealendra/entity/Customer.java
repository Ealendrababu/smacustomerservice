package com.sma.ealendra.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name="custom_tab")
@DynamicUpdate
public class Customer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="custom_id_col")
	private Long id;
	
	@Column(name="custom_name_col")
	private String name;
	
	@Column(name="custom_email_col")
	private String email;
	
	@Column(name="custom_gender_col")
	private String gender;
	
	@Column(name="custom_img_col")
	private String imagePath;
	
	@Column(name="custom_mobile_col")
	private String mobile;
	
	@Column(name="custom_addr_col")
	private String address;
	
	@Column(name="custom_pancard_col")
	private String panCardId;
	
	@Column(name="custom_aadhar_col")
	private String aadharId;
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name="addr_fk_col")
	private Address addr;
	

}
