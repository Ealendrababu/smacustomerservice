package com.sma.ealendra.service;

import java.util.List;
import java.util.Optional;

import com.sma.ealendra.entity.Customer;

public interface ICustomerService {

	public Long createCustomer(Customer customer);

	public List<Customer> getAllCustomer();

	public Customer getOneCustomer(Long id);

	public void deleteCustomer(Long id);

	public void updateCustomer(Customer customer);

	//find By email,mobile,pancard,aadharcard.

	public Customer findByEmail(String email);

	public   Customer findByMobile(String mobile);

	public Customer findByPanCardId(String panCardId);

	public  Customer findByAadharId(String aadharId);



}
