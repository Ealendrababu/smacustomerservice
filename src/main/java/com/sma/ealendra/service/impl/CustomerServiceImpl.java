package com.sma.ealendra.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sma.ealendra.entity.Customer;
import com.sma.ealendra.exception.CustomerNotFoundException;
import com.sma.ealendra.repository.CustomerRepository;
import com.sma.ealendra.service.ICustomerService;

@Service
public class CustomerServiceImpl implements ICustomerService {
	
	@Autowired
	private CustomerRepository repo;


	public Long createCustomer(Customer customer) {
		
		return repo.save(customer).getId();
	}

	
	public List<Customer> getAllCustomer() {
		
		return repo.findAll();
	}

	
	public Customer getOneCustomer(Long id) {
		
	Optional<Customer> opt=	repo.findById(id);
	if(opt.isPresent())
	{
	  return opt.get();
	}else
	{
		throw new CustomerNotFoundException("Customer '"+id+"' is Not Exist");
	}
	

	}
	public void deleteCustomer(Long id) {
		//repo.deleteById(id);
		repo.delete(getOneCustomer(id));
		
	}

	
	public void updateCustomer(Customer customer) {
		
		
	Long id=customer.getId();
	if(id!=null && repo.existsById(id))
	{
		repo.save(customer);
	}else
	{
		throw new CustomerNotFoundException("Customer '"+customer.getId()+"' Not Exist");
	}	
	}


	
	public Customer findByEmail(String email) {
		Optional<Customer> opt=repo.findByEmail(email);
		if(opt.isPresent())
		{
			return opt.get();
		}
		else throw new CustomerNotFoundException("'"+email+"' is Not Exist");
	}



	public Customer findByMobile(String mobile) {
		 Optional<Customer> opt=   repo.findByEmail(mobile);
		 if(opt.isPresent())
		 {
			 return opt.get();
		 }
		 else
			 throw new CustomerNotFoundException("Customer '"+mobile+"' Number is Not Exist");	
	}


	public Customer findByPanCardId(String panCardId) {
		
	Optional<Customer> opt=	repo.findByPanCardId(panCardId);
	if(opt.isPresent())
	{
		return opt.get();
	}else
	{
		throw new CustomerNotFoundException(" Pancard'"+panCardId+"' is not Exist");
	}		
	}


	
	public Customer findByAadharId(String aadharId) {
		
	Optional<Customer> opt=	repo.findByAadharId(aadharId);
	if(opt.isPresent())
	{
		return opt.get();
	}
	else
		throw new CustomerNotFoundException("Aadhar '"+aadharId+"' is Not Exist");
	}
	
	

}
