package com.sma.ealendra.restcontroller;

import java.util.List;

import javax.ws.rs.GET;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sma.ealendra.entity.Customer;
import com.sma.ealendra.service.ICustomerService;

@RestController
@RequestMapping("/customer")
public class CustomerRestController {
	
	@Autowired
	private ICustomerService service;
	
	@PostMapping("/create")
	public ResponseEntity<String> createCustomer(@RequestBody Customer customer)
	{
		ResponseEntity<String> resp=null;
		try {
		Long id =service.createCustomer(customer);
		String message="Customer '"+id+"' is created";
		resp = ResponseEntity.ok(message);
		} catch (Exception e) {
			e.printStackTrace();
			resp= ResponseEntity.ok(e.getMessage());
		}
		return resp;
	}
	@GetMapping("/all")
	public ResponseEntity<List<Customer>> getAllCustomer()
	{
		ResponseEntity<List<Customer>> resp=null;
		try {
			
	List<Customer>	list=service.getAllCustomer();
	resp = ResponseEntity.ok(list);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return resp;
	}
	
	@GetMapping("/find/{id}")
	public ResponseEntity<?> getOneCustomer(@PathVariable Long id)
	{
		ResponseEntity<?> resp=null;
		try {
			
	Customer customer=service.getOneCustomer(id);
	resp =new ResponseEntity<Customer>(customer,HttpStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
	resp = new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);		
			
		}
		return resp;
	}
	@DeleteMapping("/remove/{id}")
	public ResponseEntity<String> deleteCustomer(@PathVariable Long id)
	{
		ResponseEntity<String> resp=null;
		try {
		service.deleteCustomer(id);
		String message="customer '"+id+"'is deleted";
		resp=new ResponseEntity<String>(message,HttpStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
			resp = new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		return resp;
	}
	@PutMapping("/modify")
	public ResponseEntity<String> updateCustomer(@RequestBody Customer customer)
	{
		ResponseEntity<String> resp=null;
		try {
			service.updateCustomer(customer);
			String message="customer '"+customer+"'is updated!!";
			resp = new ResponseEntity<String>(message,HttpStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
		resp = new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return resp;
	}
	
	//find By email,mobile,pancard,aadharcard.
	
	@GetMapping("/findByEmail/{email}")
	public ResponseEntity<?> getByEmail(@PathVariable String email)
	{
		
		ResponseEntity<?> resp=null;
		try {
		Customer custom=service.findByEmail(email);
		resp =  new ResponseEntity<Customer>(custom,HttpStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
		resp = new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return resp;
	}
	
	@GetMapping("/findByMobile/{mobile}")
	public ResponseEntity<?> getByMobile(@PathVariable String mobile)
	{
		ResponseEntity<?> resp=null;
		try {
	Customer customer=service.findByMobile(mobile);
	//String message="This Mobile("+customer+") Number ";
	resp = new ResponseEntity<Customer>(customer,HttpStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
			resp = new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		return resp;
	}
	@GetMapping("/findByPancard/{pancard}")
	public ResponseEntity<?> getByPanCard(@PathVariable String pancard)
	{
		ResponseEntity<?> resp=null;
		try {
			
			Customer customer=service.findByPanCardId(pancard);
			//String message="The Pancard Number is'"+customer+"'";
			resp=new ResponseEntity<Customer>(customer,HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			resp = new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return resp;
	}
	
	@GetMapping("/findByAdhar/{aadhar}")
	public ResponseEntity<?> getByAadhar(@PathVariable String aadhar)
	{
		ResponseEntity<?> resp=null;
		try {
			
	Customer customer=service.findByAadharId(aadhar);
	//String message="AadharCard number is'"+customer+"'";
			resp= new ResponseEntity<Customer>(customer,HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		resp = new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);	
		}
		return resp;
	}

}
