package com.sma.ealendra.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sma.ealendra.entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

	//find By email,mobile,pancard,aadharcard.

	public Optional<Customer> findByEmail(String email);

	public  Optional<Customer> findByMobile(String mobile);

	public Optional<Customer> findByPanCardId(String panCardId);

	public Optional<Customer> findByAadharId(String aadharId);






}

